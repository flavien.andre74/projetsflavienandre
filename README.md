# Description Projets

Bonjour, je vous présente ici une brève description de chacun de mes projets.

*******
Sommaire 
 1. [ExeJeu2D](#jeu2D)
 2. [NeeDice](#NeeDice)
 3. [Reflection](#Reflection)
 4. [D&D](#D&D)
 5. [StudentJS](#StudentJS)
 6. [TowerDefense](#TowerDefense)
*******


<div id='jeu2D'/>

## **ExeJeu2D** 

Le dossier appelé ExeJeu2D est mon premier platformer 2D, réalisé en 1 mois lors de ma découverte de Unity.

J'ai juste voulu ajouter plusieurs fonctionnalités pour apprendre à me servir du moteur de jeu.

Ce jeu contient notamment :

    - Menu (Selection de niveau)
    - Paramètres (gestion volume sonore, plein écran, qualité graphique)
    - Crédit
    - Système sonore (Musique et FX)
    - Système de déplacement
    - Monstre
    - Boss
    - PNJ
    - Shop
    - Système de potion
    - Système de magie/sort
    ...



<div id='NeeDice'/>

## **NeeDice** 

NeeDice : votre lanceur de dé personnalisable !
  
Lancer de dé fait par le mouvement de votre téléphone, application utilisé pour les jeux de roles.
Utilisable par les personnes aveugle et malvoyante.

*******


### Fonctionnalités

- Personnalisation de la couleur et du nombre de faces de dé.   
- Lancement de dé fait avec l'accéléromètre.   
- Lancement de dé avec la voix   
- Annonce du résultat par la sortie audio du smartphone.  
- Mode sombre pour l'application
- Dessin sur la face du dé avec une animation d'entrée
- Plusieurs langues disponibles dans l'application
- Choix de la couleur et du dé à lancer

*******

### Lancement

Le projet est lancer sur un téléphone physique ou virtuel à partir de l'application "Android Studio"


<div id='Reflection'/>

# **Reflection**

Un jeu d'horreur 3D nommé Reflection qui m'a permis de développer un système multijoueur avec "Mirror" et une intelligence artificielle. Le jeu a été réalisé sur Unity.

C'est un projet réalisé par groupe de 2 durant ma 2e année d'études (IUT Informatique de Clermont-Ferrand).

(Le serveur sur lequel tourne la base de données du jeu est actuellement en cours de réparation, le jeu n'est actuellement pas jouable sans système de connexion, cela devrait changer dans les mois à venir)


<div id='D&D'/>

# **Discussion&Dungeon**

Une application Windows de type master detail réalisé pour un projet de 3 mois durant ma 1 ère année de BUT.

Elle est codée en C# et la partie front est réalisée avec XAML.

C'est une application qui recense différents jeux vidéo du type "MMORPG". Elle sert aux joueurs voulant en connaître plus sur leurs jeux préférés.


<div id='StudentJS'/>

# **StudentJS**

Le but de ce projet fut de développer un jeu full javascript inspiré du jeu "Mario Bros". Un mode multijoueur est disponible, utilisant des requetes NodeJS.

Pour lancer le projet : Aller dans répertoire "/src" est effectuer la commande : npm run start

Le serveur est ouvert et jouable sur l'adresse localhost:8081


<div id='TowerDefense'/>

# **TowerDefense**

Un projet réalisé en seulement 3 jours avec 3 camarades. Le but est de défendre les tours contre des vagues de monstre. 

Le jeu a été fait avec Unreal Engine 5, 100% en Blueprint.

PS : Le build est trop gros pour être push, mais le jeu est génial :)

# **Futur Projet**

Un Endless Runner (genre subway surfer) réalisé avec UE5 en C++.
Un top-down shooter développer avec Unity (version démo car la version payante sera disponible sur Steam).
